package com.victorarsjad.backendPlayground.resolver

import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Service

@Service
class HelloQueryResolver : GraphQLQueryResolver {

    fun hello(): String {
        return "Hello! Welcome to GraphQL demo"
    }
}