package com.victorarsjad.backendPlayground

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BackendPlaygroundApplication

fun main(args: Array<String>) {
	runApplication<BackendPlaygroundApplication>(*args)
}
